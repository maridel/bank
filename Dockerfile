FROM python:3.9.10-buster

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install pipenv

COPY . /app
WORKDIR /app

COPY Pipfile Pipfile.lock /app/

RUN pipenv install --system --deploy