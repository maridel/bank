import os
from os.path import dirname

import environ
from telegram.ext import Updater

from ..internal.transport.bot.handlers import handlers


def run():
    env = environ.Env(
        BOT_TOKEN=(str, ""), 
        DEBUG=(bool, False), 
        WEB_PORT=(int, 0),
        POSTGRES_USER = (str, ""), 
        POSTGRES_PASSWORD = (str, ""), 
        POSTGRES_DB = (str, ""),
        API_ID = (int, 0),
        API_HASH = (str, ""), 
    )

    BASE_DIR = dirname(dirname(dirname(dirname(os.path.abspath(__file__)))))

    if env("DEBUG"):
        environ.Env.read_env(os.path.join(BASE_DIR, ".env.dev"))
    else:
        environ.Env.read_env(os.path.join(BASE_DIR, ".env.prod"))
    token = env("BOT_TOKEN")

    updater = Updater(token=token, use_context=True)
    dispatcher = updater.dispatcher

    for h in handlers:
        dispatcher.add_handler(h)

    updater.start_polling()

    # updater.start_webhook(listen='0.0.0.0',
    #                     port=8443,
    #                     url_path="bot",
    #                 #   key='/etc/letsencrypt/live/dtbank.backend22.2tapp.cc/privkey.pem',
    #                 #   cert='/etc/letsencrypt/live/dtbank.backend22.2tapp.cc/fullchain.pem',
    #                     webhook_url='https:///botdtbank.backend22.2tapp.cc')
    
    #updater.idle()