from ..models.card import Card
from django.db.models import F
from django.db import transaction
#импорты больше чем через 1 через точку плохо

class CardService:
    def __init__(self, inp_payment_service):
        self.payment_service = inp_payment_service

    def get_cards_by_user(self, user):
        return Card.objects.filter(bank_account__owner=user).values("number", "balance_rub")

    def get_card_by_user_and_number(self, user, inp_number):
        try:
            return Card.objects.get(bank_account__owner=user, number = inp_number)
        except Card.DoesNotExist:
            return None
    
    def get_card_by_number(self, inp_number):
        try:
            return Card.objects.get(number = inp_number)
        except Card.DoesNotExist:
            return None
    
    #переименовать
    def pay(self, user, card_number1, card_number2, sum):
        card1 = self.get_card_by_user_and_number(user, card_number1)
        if card1 and card1.balance_rub >= sum:
            card2 = self.get_card_by_number(card_number2)
            if card2:
                with transaction.atomic():
                    card1.balance_rub = F('balance_rub') - sum
                    card1.save(update_fields=["balance_rub"])
                    card2.balance_rub = F('balance_rub') + sum
                    card2.save(update_fields=["balance_rub"])
                    self.payment_service.create_payment(card1, card2, sum)
                return True
        return False