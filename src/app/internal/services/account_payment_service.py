from ..models.account_payment import AccountPayment
from django.db.models import Q

class AccountPaymentService:
    
    def create_payment(self, c1, c2, sum):
        AccountPayment.objects.create(from_account = c1, 
                to_account = c2, sum_rub = sum)
    
    def get_payments_by_acc(self, c):
        return AccountPayment.objects.filter(Q(from_account__checking_account = c) | Q(to_account__checking_account = c)).select_related('from_account', 'to_account')
    
    def get_contacts_by_acc_to(self, c):
        return AccountPayment.objects.filter(from_account__checking_account = c).values_list("to_account__owner__username")
    
    def get_contacts_by_acc_from(self, c):
        return AccountPayment.objects.filter(to_account__checking_account = c).values_list("from_account__owner__username")