from ..models.token import Token

class TokenService:
    def validate(self, inp_user, inp_jti):
        return Token.objects.filter(jti = inp_jti, user = inp_user, revoked = False).exists()

    def revoke_prev(self, inp_user):
        #update
        for to_revoke in Token.objects.filter(user = inp_user, revoked = False):
            to_revoke.revoked = True
            to_revoke.save()

    def add_new(self, inp_user, inp_jti):
        Token.objects.create(user = inp_user, jti = inp_jti, revoked = False)
