from ..models.card_payment import CardPayment
from django.db.models import Q

class CardPaymentService:
    
    def create_payment(self, c1, c2, sum):
        CardPayment.objects.create(from_card = c1, 
                to_card = c2, sum_rub = sum)
    
    def get_payments_by_card(self, c):
        return CardPayment.objects.filter(Q(from_card__number = c) | Q(to_card__number = c)).select_related('from_card', 'to_card')
    
    def get_contacts_by_card_to(self, c):
        return CardPayment.objects.filter(from_card__number = c).values_list("to_card__bank_account__owner__username")
    
    def get_contacts_by_card_from(self, c):
        return CardPayment.objects.filter(to_card__number = c).values_list("from_card__bank_account__owner__username")