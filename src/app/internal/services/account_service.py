from ..models.bank_account import BankAccount
from django.db.models import F
from django.db import transaction

class AccountService:
    def __init__(self, inp_payment_service, inp_user_service):
        self.payment_service = inp_payment_service
        self.user_service = inp_user_service

    def create_account(self, user_id, dict_account):
        u = self.user_service.get_user_by_uuid(user_id)
        if u:
            m = BankAccount(owner = u, **dict_account)
            m.save()
            return True
        return False

    def update_account(self, dict_account):
        if 'checking_account' in dict_account:
            return BankAccount.objects.filter(
                checking_account = dict_account['checking_account']).update(
                **dict_account
                )
        return 0

    def get_account_by_user(self, user):
        return BankAccount.objects.filter(owner=user).values("checking_account", "balance_rub")

    def get_account_balance_by_user_and_number(self, user, inp_number):
        try:
            return BankAccount.objects.values("balance_rub").get(owner=user, checking_account = inp_number)
        except BankAccount.DoesNotExist:
            return None

    def get_account_by_user_and_number(self, user, inp_number):
        try:
            return BankAccount.objects.get(owner=user, checking_account = inp_number)
        except BankAccount.DoesNotExist:
            return None
    
    def get_account_by_number(self, inp_number):
        try:
            return BankAccount.objects.get(checking_account = inp_number)
        except BankAccount.DoesNotExist:
            return None
    
    def pay(self, user, card_number1, card_number2, sum):
        account1 = self.get_account_by_user_and_number(user, card_number1)
        if account1 and account1.balance_rub >= sum:
            account2 = self.get_account_by_number(card_number2)
            if account2:
                with transaction.atomic():
                    account1.balance_rub = F('balance_rub') - sum
                    account1.save(update_fields=["balance_rub"])
                    account2.balance_rub = F('balance_rub') + sum
                    account2.save(update_fields=["balance_rub"])
                    self.payment_service.create_payment(account1, account2, sum)
                return True
        return False