from django.db import models
from django.core.validators import RegexValidator
from app.internal.users.db.models import BankUser

class BankAccount(models.Model):
    owner = models.ForeignKey(BankUser, on_delete=models.CASCADE)
    #рассчетный счет
    checking_account = models.CharField(primary_key=True, max_length=20, 
                                        validators=[RegexValidator(r'^\d{20}$', message="20 digits required")])
    bank_identification_code = models.CharField(max_length=9, 
                                                validators=[RegexValidator(r'^\d{9}$', message="9 digits required")])
    bank_name = models.CharField(max_length=255)
    correspondent_account = models.CharField(max_length=20, 
                                             validators=[RegexValidator(r'^\d{20}$', message="20 digits required")])
    balance_rub = models.DecimalField(max_digits=10, decimal_places=2, default=0)
