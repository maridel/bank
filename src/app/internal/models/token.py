from django.db import models
from app.internal.users.db.models import BankUser

class Token(models.Model):
    user = models.ForeignKey(BankUser, related_name="token", on_delete=models.CASCADE)
    jti = models.CharField(max_length=255, primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    revoked = models.BooleanField(default=False)