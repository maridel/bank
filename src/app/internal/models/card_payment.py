from django.db import models
from app.internal.models.card import Card

class CardPayment(models.Model):
    from_card = models.ForeignKey(Card, on_delete=models.CASCADE, related_name="from_cards")
    to_card = models.ForeignKey(Card, on_delete=models.CASCADE, related_name="to_cards")
    sum_rub = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    datetime = models.DateTimeField(auto_now_add=True)