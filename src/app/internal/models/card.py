from django.db import models
from django.core.validators import RegexValidator
from datetime import timedelta
from django.db.models import DateField

from app.internal.users.db.models import BankUser
from app.internal.models.bank_account import BankAccount

#https://www.django-antipatterns.com/pattern/date-time-field-s-that-store-a-week-or-month.html
class DateTruncMixin:

    def truncate_date(self, dt):
        return dt

    def to_python(self, value):
        value = super().to_python(value)
        if value is not None:
            return self.truncate_date(value)
        return value

#в отдельный файлик
class MonthField(DateTruncMixin, DateField):

    def truncate_date(self, dt):
        return dt - timedelta(days=dt.day-1)

# class PaymentSystem(models.TextChoices):
#     Visa 

class Card(models.Model):
    number = models.CharField(primary_key=True, max_length=18, 
    validators=[RegexValidator(r'^\d{16,18}$', message="from 16 to 18 digits required")])
    safety_code = models.CharField(max_length=4, 
    validators=[RegexValidator(r'^\d{3,4}$', message="from 3 to 4 digits required")])
    validity = MonthField()
    balance_rub = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    #будем считать что у нас нет пока мультивалютных карт
    bank_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE)
    #для совершения операций это кажется хранить необязательно
    visa = "Visa"
    master_card = "MasterCard"
    mir = "МИР"
    union = "Union"
    choices_list = [
        (visa, visa), 
        (master_card, master_card), 
        (mir, mir), 
        (union, union)]
    #255 TextChoice
    max_length_choices = max(map(lambda x: len(x[0]), choices_list))
    payment_system = models.CharField(max_length=max_length_choices, choices=choices_list)
