from datetime import datetime
from django.db import models
from app.internal.models.bank_account import BankAccount

class AccountPayment(models.Model):
    from_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name="from_account")
    to_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name="to_account")
    sum_rub = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    datetime = models.DateTimeField(auto_now_add=True)