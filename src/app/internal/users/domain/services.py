from ..db.models import BankUser


class UserService:
    #gets - user: объект telegram.User
    #https://python-telegram-bot.readthedocs.io/en/stable/telegram.user.html#telegram-user
    #returns - (obj: BankUser, created: Boolean)
    def add_or_update_user(self, user):
        dict_user = user.to_dict()
        common_fields = ["id", "first_name", "last_name", "username"]
        dict_user_filtered = {k: v for k, v in dict_user.items() if v is not None and k in common_fields}
        return BankUser.objects.update_or_create(
            id=user.id,
            defaults=dict_user_filtered,
            )
    
    def add_or_update_user_new(self, user):
        dict_user = user.dict()
        common_fields = ["id", "first_name", "last_name", "username"]
        dict_user_filtered = {k: v for k, v in dict_user.items() if v is not None and k in common_fields}
        return BankUser.objects.update_or_create(
            id=user.id,
            defaults=dict_user_filtered,
            )
    
    def update_password(self, inp_username, inp_password):
        BankUser.objects.filter(username=inp_username).update(password = inp_password)

    def add_or_update_phone(self, inp_id, phone):
        BankUser.objects.filter(id=inp_id).update(phone_number = phone)

    def check_user_exists(self, inp_id):
        return BankUser.objects.filter(id=inp_id).exists()
    
    def check_user_with_phone_exists(self, inp_id):
        return BankUser.objects.exclude(phone_number__isnull=True).filter(id = inp_id).exists()

    def get_user_by_id(self, inp_id):
        try:
            return BankUser.objects.get(id=inp_id)
        except BankUser.DoesNotExist:
            return None
    
    def get_user_by_username(self, inp_username):
        try:
            return BankUser.objects.get(username=inp_username)
        except BankUser.DoesNotExist:
            return None
    
    def get_user_by_username_prefetch(self, inp_username):
        try:
            return BankUser.objects.get(username=inp_username).prefetch_related('fav_users')
        except BankUser.DoesNotExist:
            return None
    
    def add_to_fav(self, user, username):
        u = self.get_user_by_username(username)
        if u:
            user.fav_users.add(u)
            user.save()
            return True
        return False
    
    def remove_fav(self, user, username):
        u = self.get_user_by_username(username)
        if u:
            user.fav_users.remove(u)
            user.save()
            return True
        return False
    
    def list_fav(self, user):
        return user.fav_users.all()
    
    def get_user_by_uuid(self, inp_uuid):
        try:
            return BankUser.objects.get(uuid=str(inp_uuid))
        except BankUser.DoesNotExist:
            return None
    
    def delete_user_by_uuid(self, inp_uuid):
        try:
            BankUser.objects.get(uuid=str(inp_uuid)).delete()
            return True
        except BankUser.DoesNotExist:
            return False
