from ninja.orm import create_schema
from ninja import Schema
from ..db.models import BankUser

UserSchema = create_schema(BankUser, exclude = ['password', 'fav_users'])

class UserOut(UserSchema):
    pass
class UserIn(UserSchema):
    pass