from tokenize import Token
from django.core.validators import RegexValidator
from django.db import models

import uuid

class BankUser(models.Model):
    phone_regex = RegexValidator(
        regex=r"^\+\d{8,15}$",
        message="The phone number must be in the format: '+999999999' and contain up to 15 digits",
    )
    password = models.CharField(max_length=255, blank=True, null=True)
    #создается по дефолту
    id = models.IntegerField(primary_key=True)
    uuid = models.CharField(max_length=32, unique=True, default=uuid.uuid4().hex)
    phone_number = models.CharField(validators=[phone_regex], max_length=16, blank=True, null=True, unique=True,)
    first_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255, blank=True)
    username = models.CharField(max_length=255, blank=True, null=True, unique=True)
    fav_users = models.ManyToManyField('self', blank=True)
    #часть реквизитов для счета, но логичнее хранить тут
    inn = models.CharField(max_length=12, 
    validators=[RegexValidator(regex=r'^\d{12}$', message="12 digits required")], 
    blank=True, null=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    class Meta:
        #app_label = 'app'
        verbose_name = "BankUser"
        verbose_name_plural = "BankUsers"