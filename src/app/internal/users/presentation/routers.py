from config.entities import Message
from ninja import Router
from ..domain.entities import UserOut

def get_users_router(user_handlers):
    router = Router(tags = ['users'])

    router.add_api_operation(
        '/{user_id}',
        ['GET'],
        user_handlers.get_user_by_id,
        response = {200: UserOut, 404:Message}
    )

    router.add_api_operation(
        '',
        ['PUT'],
        user_handlers.put_user,
        response = {200: UserOut}
    )

    router.add_api_operation(
        '/{user_id}',
        ['DELETE'],
        user_handlers.delete_user_by_id,
        response = {200: bool}
    )

    return router