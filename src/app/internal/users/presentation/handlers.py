from ..domain.entities import UserOut, UserIn

class UserHandlers:
    def __init__(self, user_service):
        self._user_service = user_service

    def get_user_by_id(self, request, user_id):
        user= self._user_service.get_user_by_uuid(user_id)
        if user:
            return UserOut.from_orm(user)
        else:
            return 404, {'message': 'NotFound'}
    
    def put_user(self, request, user: UserIn):
        return UserOut.from_orm(self._user_service.add_or_update_user_new(user)[0])
    
    def delete_user_by_id(self, request, user_id):
        return 200, self._user_service.delete_user_by_uuid(user_id)