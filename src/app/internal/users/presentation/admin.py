from django.contrib import admin

from ..db.models import BankUser


@admin.register(BankUser)
class BankUserAdmin(admin.ModelAdmin):
    pass