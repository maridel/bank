from tkinter.messagebox import NO
from turtle import up
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

import telegram
from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, MessageHandler
from telegram.ext.filters import Filters

from app.internal.models.admin_user import AdminUser

from ...users.domain.services import UserService
from ...services.cards_service import CardService
from ...services.account_service import AccountService
from ...services.card_payment_service import CardPaymentService
from ...services.account_payment_service import AccountPaymentService

bot_card_payment_service = CardPaymentService()
bot_card_acc_payment_service = AccountPaymentService()
bot_user_service = UserService()
bot_cards_service = CardService(bot_card_payment_service)
bot_account_service = AccountService(bot_card_acc_payment_service, bot_user_service)

USER_SAVED = "User is saved in DB"
SHARE_CONTACT_AGREE = "I agree to share contact"
SHARE_CONTACT_ASK = "Would you mind sharing your contact with me?"
NEED_START = "Before adding phone /start conversation"
NEED_PHONE = "You can not use this command before adding a phone number"
NO_ACCOUNT = "User with given id has no account with given number"
WRONG_ACCOUNT_FORMAT = "Please provide account number in format /account_balance account_number"
NO_CARDS = "No available cards"
CHOOSE_CARD = "Choose from available cards"
PHONE_UPDATED = "Phone is updated"

def start(update: Update, context: CallbackContext):
    bot_user_service.add_or_update_user(update._effective_user)
    context.bot.send_message(chat_id=update.effective_chat.id, text=USER_SAVED)


start_handler = CommandHandler("start", start)


def ask_phone(update: Update, context: CallbackContext):
    user = bot_user_service.check_user_exists(update._effective_user.id)
    if user:
        contact_keyboard = telegram.KeyboardButton(text=SHARE_CONTACT_AGREE, request_contact=True)
        custom_keyboard = [[contact_keyboard]]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard)
        context.bot.send_message(
            chat_id=update.effective_user.id,
            text=SHARE_CONTACT_ASK,
            reply_markup=reply_markup,
        )
    else:
        context.bot.send_message(chat_id=update.effective_user.id, text=NEED_START)


set_phone_handler = CommandHandler("set_phone", ask_phone)


def add_phone(update: Update, context: CallbackContext):
    bot_user_service.add_or_update_phone(update._effective_user.id, update.message.contact.phone_number)
    reply_markup = telegram.ReplyKeyboardRemove()
    context.bot.send_message(chat_id=update.effective_user.id, text=PHONE_UPDATED, reply_markup=reply_markup)


add_phone_handler = MessageHandler(Filters.contact, add_phone)


def mask_empty(value):
    if value == "": 
        return "-"
    return value


def tg_str(user):
    name = mask_empty(user.first_name)
    surname = mask_empty(user.last_name)
    phone = mask_empty(user.phone_number)
    return f"Id: {user.id}\nName: {name}\nSurname: {surname}\nPhone: {phone}"

def require_user_with_phone(update: Update, context: CallbackContext, action):
    user = bot_user_service.get_user_by_id(update.effective_user.id)
    if user and user.phone_number:
        action(user, context)
    else:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text=NEED_PHONE
        )

def me(update: Update, context: CallbackContext):
    action = lambda u, c: context.bot.send_message(chat_id=update.effective_chat.id, text=f"{tg_str(u)}")
    require_user_with_phone(update, context, action)

me_handler = CommandHandler("me", me)

def card_balance_action(u, context):
    cards = bot_cards_service.get_cards_by_user(u)
    custom_keyboard = []
    i = 1
    for card in cards:
        card_string = f"{i}: {str(card['number'])[-4:]}"
        context.chat_data[card_string] = card['balance_rub']
        contact_keyboard = telegram.KeyboardButton(text=card_string)
        custom_keyboard.append([contact_keyboard])
        i+=1
        
    if len(custom_keyboard) == 0:
        context.bot.send_message(
        chat_id=u.id,
        text=NO_CARDS,
        )
    else:    
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, one_time_keyboard=True)
        context.bot.send_message(
            chat_id=u.id,
            text=CHOOSE_CARD,
            reply_markup=reply_markup,
        )

def card_balance(update: Update, context: CallbackContext):
    require_user_with_phone(update, context, card_balance_action)


card_handler = CommandHandler("card_balance", card_balance)

def chosen_card(update: Update, context: CallbackContext):
    #возможна KeyError
    context.bot.send_message(chat_id=update.effective_chat.id, text=f"{context.chat_data[update.message.text]}")

chosen_card_handler = MessageHandler(Filters.regex(r'\d: \d{4}'), callback=chosen_card)

def account_balance_action(u, context):
    if len(context.args) == 1:
        account = bot_account_service.get_account_balance_by_user_and_number(u, context.args[0])
        if account:
            context.bot.send_message(chat_id=u.id, text=f"{account['balance_rub']}")
        else:
            context.bot.send_message(chat_id=u.id, 
            text=NO_ACCOUNT)
    else:
        context.bot.send_message(
                chat_id=u.id, 
                text=WRONG_ACCOUNT_FORMAT
            )

def account_balance(update: Update, context: CallbackContext):
    require_user_with_phone(update, context, account_balance_action)


account_handler = CommandHandler("account_balance", account_balance)

def pay_card(update: Update, context: CallbackContext):
    def pay_action(u, context):
        if bot_cards_service.pay(u, context.args[0], context.args[1], float(context.args[2])):
            context.bot.send_message(chat_id=update.effective_chat.id, text=f"Success")
        else:
            context.bot.send_message(chat_id=update.effective_chat.id, text=f"Error")
    require_user_with_phone(update, context, pay_action)

pay_card_handler = CommandHandler("pay_card", pay_card)

def pay_balance(update: Update, context: CallbackContext):
    def pay_action(u, context):
        bot_account_service.pay(u, context.args[0], context.args[1], float(context.args[2]))
        context.bot.send_message(chat_id=update.effective_chat.id, text=f"Success")
    require_user_with_phone(update, context, pay_action)

pay_account_handler = CommandHandler("pay_account", pay_balance)

def pay_user(update: Update, context: CallbackContext):
    def pay_action(u, context):
        user_to = bot_user_service.get_user_by_username(context.args[1])
        if user_to:
            cards = bot_cards_service.get_cards_by_user(user_to)
            if bot_cards_service.pay(u, context.args[0], cards[0]['number'], float(context.args[2])):
                context.bot.send_message(chat_id=update.effective_chat.id, text=f"Success")
            else:
                context.bot.send_message(chat_id=update.effective_chat.id, text=f"Error")
        else:
            context.bot.send_message(chat_id=update.effective_chat.id, text=f"No such user")
    require_user_with_phone(update, context, pay_action)

pay_user_handler = CommandHandler("pay_user", pay_user)

def add_fav(update: Update, context: CallbackContext):
    def action(u, context):
        if bot_user_service.add_to_fav(u, context.args[0]):
            context.bot.send_message(chat_id=update.effective_chat.id, text=f"Success")
        else:
            context.bot.send_message(chat_id=update.effective_chat.id, text=f"No such user")
    require_user_with_phone(update, context, action)

add_fav_handler = CommandHandler("add_to_favorites", add_fav)

def del_fav(update: Update, context: CallbackContext):
    def action(u, context):
        if bot_user_service.remove_fav(u, context.args[0]):
            context.bot.send_message(chat_id=update.effective_chat.id, text=f"Success")
        else:
            context.bot.send_message(chat_id=update.effective_chat.id, text=f"No such user")
    require_user_with_phone(update, context, action)

del_fav_handler = CommandHandler("delete_from_favorites", del_fav)

def list_fav(update: Update, context: CallbackContext):
    def action(u, context):
        res = []
        for fu in bot_user_service.list_fav(u):
            res.append(f"{fu.first_name} {fu.last_name}")
        if len(res) != 0:
            result = '\n'.join(set(res))
            context.bot.send_message(chat_id=update.effective_chat.id, 
                text=f"{result}")
        else:
            context.bot.send_message(chat_id=update.effective_chat.id, 
                text=f"empty")
    require_user_with_phone(update, context, action)

list_fav_handler = CommandHandler("favorites", list_fav)

def payment_list_card(update: Update, context: CallbackContext):
    def action(u, context):
        res = []
        for payment in bot_card_payment_service.get_payments_by_card(context.args[0]):
            res.append(f"{payment.datetime} {payment.from_card.number} {payment.to_card.number} {payment.sum_rub}")
        if len(res) != 0:
            result = '\n'.join(set(res))
            context.bot.send_message(chat_id=update.effective_chat.id, 
            text=f"{result}")
        else:
            context.bot.send_message(chat_id=update.effective_chat.id, 
                text=f"empty")
    require_user_with_phone(update, context, action)

payment_list_card_handler = CommandHandler("payment_list", payment_list_card)

def payment_list_card_acc(update: Update, context: CallbackContext):
    def action(u, context):
        res = []
        for payment in bot_card_acc_payment_service.get_payments_by_acc(context.args[0]):
            res.append(f"{payment.datetime} {payment.from_account.checking_account} {payment.to_account.checking_account} {payment.sum_rub}")
        if len(res) != 0:
            result = '\n'.join(set(res))
            context.bot.send_message(chat_id=update.effective_chat.id, 
            text=f"{result}")
        else:
            context.bot.send_message(chat_id=update.effective_chat.id, 
                text=f"empty")
    require_user_with_phone(update, context, action)

payment_list_card_acc_handler = CommandHandler("payment_list_acc", payment_list_card_acc)

def contacts_list(update: Update, context: CallbackContext):
    def action(u, context):
        res = []
        for my_card in bot_cards_service.get_cards_by_user(u):
            for username in bot_card_payment_service.get_contacts_by_card_to(my_card['number']):
                res.append(username[0])
            for username in bot_card_payment_service.get_contacts_by_card_from(my_card['number']):
                res.append(username[0])
        for my_acc in bot_account_service.get_account_by_user(u):
            for username in bot_card_acc_payment_service.get_contacts_by_acc_to(my_acc['checking_account']):
                res.append(username[0])
            for username in bot_card_acc_payment_service.get_contacts_by_acc_from(my_acc['checking_account']):
                res.append(username[0])
        if len(res) != 0:
            result = '\n'.join(set(res))
            context.bot.send_message(chat_id=update.effective_chat.id, 
            text=f"{result}")
        else:
            context.bot.send_message(chat_id=update.effective_chat.id, 
                text=f"empty")
    require_user_with_phone(update, context, action)

contacts_list_handler = CommandHandler("contacts_list", contacts_list)

handlers = [start_handler, set_phone_handler, 
add_phone_handler, me_handler, card_handler, account_handler, chosen_card_handler,
pay_user_handler, pay_account_handler, pay_card_handler, add_fav_handler, del_fav_handler, 
list_fav_handler, payment_list_card_handler, contacts_list_handler, payment_list_card_acc_handler]
