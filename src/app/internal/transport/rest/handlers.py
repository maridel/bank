from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views import View
from django.forms.models import model_to_dict

from app.internal.users.domain.services import UserService
from app.internal.services.token_service import TokenService
import jwt
import bcrypt
from datetime import datetime

rest_user_service = UserService()
rest_token_service = TokenService()

import environ
import os
from os.path import dirname

env = environ.Env(
        BOT_TOKEN=(str, ""), 
        DEBUG=(bool, False), 
        WEB_PORT=(int, 0),
        POSTGRES_USER = (str, ""), 
        POSTGRES_PASSWORD = (str, ""), 
        POSTGRES_DB = (str, ""),
        SECRET_KEY = (str, ""),
        API_ID = (int, 0),
        API_HASH = (str, ""),
        SECRET = (str, ""),
    )

ENV_BASE_DIR = dirname(dirname(dirname(dirname(dirname(os.path.abspath(__file__))))))

if env("DEBUG"):
    environ.Env.read_env(os.path.join(ENV_BASE_DIR, ".env.dev"))
else:
    environ.Env.read_env(os.path.join(ENV_BASE_DIR, ".env.prod"))

secret = env('SECRET')

class BankUserView(View):
    
    def get(self, request):
        encoded_token = request.COOKIES.get('access')
        try:
            payload_dict = jwt.decode(encoded_token, secret, algorithms = "HS256")
            date = datetime.fromisoformat(payload_dict['createdAt'])
            if (datetime.now() - date).seconds < 10:
                user = rest_user_service.get_user_by_uuid(payload_dict['uuid'])
                if user and user.phone_number:
                    return  JsonResponse(model_to_dict(user, 
                                         fields = ("id", "first_name", "last_name", "phone_number")), 
                                         safe=False)
                elif user:
                    return HttpResponse("<h1>Add phone number</h1>", status = 401)
            else:
                return HttpResponse("<h1>Expired token</h1>", status = 404)    
        except:
            return HttpResponse("<h1>Not valid token</h1>", status = 404)
        return HttpResponse("<h1>User not found</h1>", status = 404)

class RefreshView(View):

    def get(self, request):
        encoded_token = request.COOKIES.get('refresh')
        payload_dict = None
        try:
            payload_dict = jwt.decode(encoded_token, secret, algorithms = "HS256")
        except:
            return HttpResponse("<h1>Not valid token</h1>", status = 404)
        if payload_dict:
            user = rest_user_service.get_user_by_uuid(payload_dict['uuid'])
            if user and rest_token_service.validate(user, encoded_token):
                datetime_now = datetime.now()
                acc_token = jwt.encode({"uuid": user.uuid, "createdAt": datetime_now.isoformat()}, secret, algorithm = "HS256")
                ref_token = jwt.encode({"uuid": user.uuid, "createdAt": datetime_now.isoformat()}, secret, algorithm = "HS256")
                rest_token_service.revoke_prev(user)
                rest_token_service.add_new(user, ref_token)
                resp = JsonResponse({"access": acc_token, "refresh": ref_token})
                resp.set_cookie('access', acc_token)
                return resp
        return HttpResponse("<h1>User not found</h1>", status = 404)

class LoginView(View):

    def get(self, request):
        user = rest_user_service.get_user_by_username(request.GET['login'])
        password = request.GET['pass']
        if user and user.password:
            string_password = user.password
            if bcrypt.checkpw(password.encode('utf8'), string_password.encode('utf8')):
                datetime_now = datetime.now()
                acc_token = jwt.encode({"uuid": user.uuid, "createdAt": datetime_now.isoformat()}, secret, algorithm = "HS256")
                ref_token = jwt.encode({"uuid": user.uuid, "createdAt": datetime_now.isoformat()}, secret, algorithm = "HS256")
                rest_token_service.revoke_prev(user)
                rest_token_service.add_new(user, ref_token)
                resp = JsonResponse({"access": acc_token, "refresh": ref_token})
                resp.set_cookie('access', acc_token)
                return resp
        return HttpResponse("<h1>User not found</h1>", status = 404)

class RegisterView(View):

    def get(self, request):
        user = rest_user_service.get_user_by_username(request.GET['login'])
        if user:
            password = request.GET['pass']
            hashed_password = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())
            rest_user_service.update_password(user.username, hashed_password.decode('utf8'))
            datetime_now = datetime.now()
            acc_token = jwt.encode({"uuid": user.uuid, "createdAt": datetime_now.isoformat()}, secret, algorithm = "HS256")
            #сделать разными
            ref_token = jwt.encode({"uuid": user.uuid, "createdAt": datetime_now.isoformat()}, secret, algorithm = "HS256")
            rest_token_service.revoke_prev(user)
            rest_token_service.add_new(user, ref_token)
            resp = JsonResponse({"access": acc_token, "refresh": ref_token})
            resp.set_cookie('access', acc_token)
            return resp
        return HttpResponse("<h1>User not found</h1>", status = 404)

def home_page_view(request):
    return render(request, "home.html")
