from django.contrib import admin

from app.internal.models.token import Token


@admin.register(Token)
class TokenAdmin(admin.ModelAdmin):
    pass
