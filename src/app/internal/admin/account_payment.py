from django.contrib import admin

from app.internal.models.account_payment import AccountPayment


@admin.register(AccountPayment)
class AccountPaymentAdmin(admin.ModelAdmin):
    pass