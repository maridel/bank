from django.contrib import admin

from app.internal.models.card_payment import CardPayment


@admin.register(CardPayment)
class CardPaymentAdmin(admin.ModelAdmin):
    pass