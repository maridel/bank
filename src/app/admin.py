from django.contrib import admin

from app.internal.admin.admin_user import AdminUserAdmin
from .internal.users.presentation.admin import BankUserAdmin
from app.internal.admin.bank_account import BankAccountAdmin
from app.internal.admin.card import CardAdmin
from app.internal.admin.card_payment import CardPaymentAdmin
from app.internal.admin.account_payment import AccountPaymentAdmin
from app.internal.admin.token import Token

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
