from telethon import TelegramClient
import os
from os.path import dirname

import environ

env = environ.Env(
        BOT_TOKEN=(str, ""), 
        DEBUG=(bool, False), 
        WEB_PORT=(int, 0),
        POSTGRES_USER = (str, ""), 
        POSTGRES_PASSWORD = (str, ""), 
        POSTGRES_DB = (str, ""),
        API_ID = (int, 0),
        API_HASH = (str, ""), 
    )

BASE_DIR = dirname(dirname(dirname(dirname(os.path.abspath(__file__)))))

environ.Env.read_env(os.path.join(BASE_DIR, ".env.dev"))

api_id = env("API_ID")
api_hash = env("API_HASH")

print(api_id)

# api_id = 14224161
# api_hash = '698391455cb6236da56e07c884eea831'

with TelegramClient('anon', api_id, api_hash) as client:
    client.loop.run_until_complete(client.send_message('@DT2BankBot', '/start'))