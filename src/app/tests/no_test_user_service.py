from django.test import TestCase
import pytest
from app.internal.services.user_service import UserService
from unittest import mock

class UserServiceTest(TestCase):

    service = UserService()

    def test_create_user(self):
        user = mock.Mock()
        user.to_dict.return_value  = {"id" : 1, "first_name": 'Test', "username": 'Test'}
        user.id = 1
        self.service.add_or_update_user(user)
        res_user = self.service.get_user_by_id(1)
        assert res_user.username == 'Test' and res_user.first_name == 'Test' and res_user.phone_number == None
        self.service.add_or_update_phone(1, '+79041672981')
        res_user = self.service.get_user_by_id(1)
        assert res_user.username == 'Test' and res_user.first_name == 'Test' and res_user.phone_number == '+79041672981'
    
    def test_update_phone(self):
        user = mock.Mock()
        user.to_dict.return_value  = {"id" : 1, "first_name": 'Test', "username": 'Test'}
        user.id = 1
        self.service.add_or_update_user(user)
        res_user = self.service.get_user_by_id(1)
        assert res_user.username == 'Test' and res_user.first_name == 'Test'
    
    def test_get_user(self):
        res_user = self.service.get_user_by_id(1)
        assert res_user == None
    
    def test_update_user(self):
        user = mock.Mock()
        user.to_dict.return_value  = {"id" : 1, "first_name": 'Test', "username": 'Test'}
        user.id = 1
        self.service.add_or_update_user(user)
        res_user = self.service.get_user_by_id(1)
        assert res_user.username == 'Test' and res_user.first_name == 'Test'
        user1 = mock.Mock()
        user1.to_dict.return_value  = {"id" : 1, "first_name": 'Test1', "username": 'Test1'}
        user1.id = 1
        self.service.add_or_update_user(user1)
        res_user1 = self.service.get_user_by_id(1)
        assert res_user1.username == 'Test1' and res_user1.first_name == 'Test1'