from app.internal.models.bank_account import BankAccount
from django.test import TestCase
import pytest
from app.internal.services.account_service import AccountService
from app.internal.services.user_service import UserService
from app.internal.models.bank_user import BankUser
from unittest import mock

class AccountServiceTest(TestCase):

    service = UserService()
    cards_service = AccountService()

    def test_get_no_acc(self):
        acc = self.cards_service.get_account_by_number("12345123451234512345")
        assert acc == None

    def test_get_acc(self):
        user = mock.Mock()
        user.to_dict.return_value  = {"id" : 1, "first_name": 'Test', "username": 'Test'}
        user.id = 1
        self.service.add_or_update_user(user)
        res_user = self.service.get_user_by_id(1)
        c = BankAccount(owner = res_user, checking_account = "12345123451234512345", 
        bank_identification_code = '123123123', bank_name = 'cool', 
        correspondent_account = "12345123451234512346", balance_rub = 100)
        c.save()
        acc1 = self.cards_service.get_account_by_number("12345123451234512345")
        acc2 = self.cards_service.get_account_by_user_and_number(res_user, "12345123451234512345")
        res_user1 = BankUser(id = 2, uuid = '123')
        res_user1.save()
        acc3 = self.cards_service.get_account_by_user_and_number(res_user1, "12345123451234512345")
        assert acc1.checking_account == "12345123451234512345" and acc2.checking_account == "12345123451234512345" and acc3 == None


