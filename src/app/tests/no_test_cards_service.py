from django.test import TestCase
import pytest
from app.internal.services.cards_service import CardService
from app.internal.services.user_service import UserService
from unittest import mock
from app.internal.models.card import Card

class CardsServiceTest(TestCase):

    service = UserService()
    cards_service = CardService()

    def test_get_no_cards(self):
        user = mock.Mock()
        user.to_dict.return_value  = {"id" : 1, "first_name": 'Test', "username": 'Test'}
        user.id = 1
        self.service.add_or_update_user(user)
        res_user = self.service.get_user_by_id(1)
        cards = self.cards_service.get_cards_by_user(res_user)
        assert cards.count() == 0