from ninja.orm import create_schema
from ninja import Schema

from app.internal.models.bank_account import BankAccount

AccountSchema = create_schema(BankAccount, exclude = ['owner'])

class AccountOut(AccountSchema):
    pass
class AccountIn(AccountSchema):
    pass

class Message(Schema):
    message: str