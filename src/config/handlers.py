from config.entities import AccountOut, AccountIn

class AccountHandlers:
    def __init__(self, acc_service):
        self._acc_service = acc_service

    def get_account_by_number(self, request, number):
        acc = self._acc_service.get_account_by_number(number)
        if acc:
            return AccountOut.from_orm(acc)
        else:
            return 404, {'message': 'NotFound'}
    
    def create_account(self, request, user_id, account: AccountIn):
        return self._acc_service.create_account(user_id, account.dict())

    def put_account(self, request, account: AccountIn):
        row_count = self._acc_service.update_account(account.dict())
        return bool(row_count) 