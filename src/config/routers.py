from config.entities import Message, AccountOut
from ninja import Router

def get_accounts_router(acc_handlers):
    router = Router(tags = ['accounts'])

    router.add_api_operation(
        '/{number}',
        ['GET'],
        acc_handlers.get_account_by_number,
        response = {200: AccountOut, 404:Message}
    )

    router.add_api_operation(
        '',
        ['POST'],
        acc_handlers.create_account,
        response = {200: bool}
    )

    router.add_api_operation(
        '',
        ['PUT'],
        acc_handlers.put_account,
        response = {200: bool}
    )

    return router