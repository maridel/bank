from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.contrib.auth import views as auth_views

from app.internal.transport.rest.handlers import BankUserView, LoginView, RefreshView, RegisterView, home_page_view
from ninja import NinjaAPI
from app.internal.users.domain.services import UserService
from app.internal.users.presentation.handlers import UserHandlers
from app.internal.users.presentation.routers import get_users_router
from app.internal.services.account_payment_service import AccountPaymentService
from app.internal.services.account_service import AccountService
from config.handlers import AccountHandlers
from config.routers import get_accounts_router

api = NinjaAPI()

user_service = UserService()
p_service = AccountPaymentService()
account_service = AccountService(p_service, user_service)

user_handlers = UserHandlers(user_service)
account_handlers = AccountHandlers(account_service)

users_handler = get_users_router(user_handlers)
account_handler = get_accounts_router(account_handlers)

api.add_router('users', users_handler)
api.add_router('accounts', account_handler)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("me/", BankUserView.as_view()),
    path("login/", LoginView.as_view()),
    path("refresh/", RefreshView.as_view()),
    path("register/", RegisterView.as_view()),
    path("api/", api.urls),
    path("", home_page_view),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
