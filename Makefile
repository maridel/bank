smoke:
	docker-compose --env-file=.env.dev -f docker-compose.yml -f docker-compose-dev.yml up -d bot
	python src/app/tests/dev_bot_smoke.py
	docker-compose --env-file=.env.dev -f docker-compose.yml -f docker-compose-dev.yml down

test:
	docker-compose --env-file=.env.dev -f docker-compose.yml -f docker-compose-dev.yml run web pytest

prod_test:
	docker-compose --env-file=.env.prod -f docker-compose.yml -f docker-compose-prod.yml run web pytest

test_print:
	docker-compose --env-file=.env.dev -f docker-compose.yml -f docker-compose-dev.yml run web pytest -s

build:
	docker image build -t 'marideldaicher/myapp:1' .

push:
	docker push 'marideldaicher/myapp:1'

prod_pull:
	docker-compose --env-file=.env.prod -f docker-compose.yml -f docker-compose-prod.yml pull

dev:
	docker-compose --env-file=.env.dev -f docker-compose.yml -f docker-compose-dev.yml up

dev_down:
	docker-compose --env-file=.env.dev -f docker-compose.yml -f docker-compose-dev.yml down

prod:
	docker-compose --env-file=.env.prod -f docker-compose.yml -f docker-compose-prod.yml up

prod_down:
	docker-compose --env-file=.env.prod -f docker-compose.yml -f docker-compose-prod.yml down

prod_recreate:
	docker-compose --env-file=.env.prod -f docker-compose.yml -f docker-compose-prod.yml up\
	 --force-recreate

prod_migrate_wrong:
	docker-compose --env-file=.env.prod -f docker-compose.yml -f docker-compose-prod.yml run web bash -c " python src/manage.py makemigrations && python src/manage.py migrate"

prod_migrate:
	docker-compose --env-file=.env.prod -f docker-compose.yml -f docker-compose-prod.yml run web python src/manage.py migrate

migrate:
	docker-compose --env-file=.env.dev -f docker-compose.yml -f docker-compose-dev.yml run web python src/manage.py migrate $(if $m, api $m,)

makemigrations:
	docker-compose --env-file=.env.dev -f docker-compose.yml -f docker-compose-dev.yml run web python src/manage.py makemigrations

createsuperuser:
	docker-compose --env-file=.env.dev -f docker-compose.yml -f docker-compose-dev.yml run web python src/manage.py createsuperuser

collectstatic:
	docker-compose --env-file=.env.dev -f docker-compose.yml -f docker-compose-dev.yml run web python src/manage.py collectstatic --no-input

app:
	docker-compose --env-file=.env.dev -f docker-compose.yml -f docker-compose-dev.yml up web

bot:
	docker-compose --env-file=.env.dev -f docker-compose.yml -f docker-compose-dev.yml up bot

#доделать
command:
	docker-compose run web python src/manage.py ${c}

shell:
	docker-compose run web python src/manage.py shell

debug:
	docker-compose run web python src/manage.py debug

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .
